'use strict';

module.exports = function (grunt) {

  grunt.initConfig({
    protractor: {
      options: {
        configFile: 'conf.js',
        keepAlive: true, // If false, the grunt process stops when the test fails.
        noColor: false, // If true, protractor will not use colors in its output.
        args: {
          // Arguments passed to the command
        }
      },
      default: {
        options: {
          args: {

          }
        }
      },
      chrome: {
        options: {
          args: {
            browser: "chrome"
          }
        }
      },
      safari: {
        options: {
          args: {
            browser: "safari"
          }
        }
      },
      firefox: {
        options: {
          args: {
            browser: "firefox"
          }
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-protractor-runner');
  grunt.registerTask('protractor-default', ['protractor:default']);
  grunt.registerTask('protractor-chrome', ['protractor:chrome']);
  grunt.registerTask('protractor-safari', ['protractor:safari']);
  grunt.registerTask('protractor-firefox', ['protractor:firefox']);
};

