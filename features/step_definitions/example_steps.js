var PhoneListing = require('../support/components/PhoneListing');
var PhoneListingPage = require('../support/pages/PhoneListingPage');
var Q = require('q');

module.exports = function() {

  var phoneListingPage = new PhoneListingPage();

  this.Given(/^I go the angular phonecat site$/, function (next) {
    // As the property 'baseUrl' has been specified in conf.js any calls to .get() will be relative to value set in 'baseUrl',
    // in this case, 'http://localhost:8000'

    // After you have finished a step definition you need to call the callback 'next'. In this step definition because there is
    // an async call/promise you should use the notify() method passing 'next' as the argument.

    browser.get('app/index.html').should.notify(next);
  });


  this.Then(/^the page title should be '(.*)' \(using plain old chai to make the assertion\)$/, function (expectedTitle, next) {
    // In the following example the promise's then() method is called. The first argument (function) in the then() method is called
    // when the promise is fulfilled. If you supply a second argument (function), this will be called when the promised is rejected.

    return browser.driver.getTitle()
      .then(function(title){
        return title.should.equal(expectedTitle);
      }).should.notify(next);
  });


  this.Then(/^the page title should be '(.*)' \(using chai-as-promised to make the assertion\)$/, function (expectedTitle, next) {
    // chai-as-promised avoids the need to handle a promise's fulfilled and rejected handlers by using the method 'eventually'

    browser.driver.getTitle().should.eventually.equal(expectedTitle).and.notify(next);
  });


  this.Given(/^I am on the angular phonecat site$/, function (next) {
    browser.driver.getTitle().should.eventually.equal('Google Phone Gallery').and.notify(next);
  });


  this.Then(/^the phone '(.*)' is displayed with description '(.*)' \(explicit\)$/, function (phoneName, phoneDescription, next) {
    element(by.cssContainingText('.phone-listing', phoneName)).isDisplayed().should.eventually.be.true
      .then(function(){
        element(by.cssContainingText('.phone-listing', phoneName)).element(by.binding('phone.snippet')).getText().should.eventually.equal(phoneDescription);
      }).should.notify(next);
  });


  this.Then(/^the phone '(.*)' is displayed with description '(.*)' \(component\/page object approach\)$/, function (phoneName, phoneDescription, next) {
    var phoneListing = new PhoneListing(phoneName);
    phoneListing.isDisplayed().should.eventually.be.true
      .then(function(){
        phoneListing.getDescription().should.eventually.equal(phoneDescription);
      }).should.notify(next);
  });


  this.Then(/^the following phones are listed in the order stated \(explicit: option 1\):$/, function(dataTable, next){
    var phonesDisplayed = [];
    element.all(by.css('.phone-listing')).each(function(elem){
       elem.element(by.binding('phone.name')).getText()
         .then(function(phoneName){
           phonesDisplayed.push(phoneName);
         });
    })
    .then(function(){
      dataTable.hashes().forEach(function(row, index){
        row.phoneName.should.equal(phonesDisplayed[index]);
      });
    }).should.notify(next);
  });


  this.Then(/^the following phones are listed in the order stated \(explicit: option 2\):$/, function(dataTable, next){
    var phonesDisplayed = element.all(by.css('.phone-listing')).map(function(elem){
      return elem.element(by.binding('phone.name')).getText();
    });
    var phonesExpected = dataTable.hashes().map(function(elem){
      return elem.phoneName;
    });

    phonesDisplayed.should.eventually.deep.equal(phonesExpected).should.notify(next);
  });


  this.Then(/^the following phones are listed in the order stated \(component\/page object approach\):$/, function(dataTable, next){
    var promises = [];
    phoneListingPage.getPhoneListings()
    .then(function(phonesDisplayed){
      dataTable.hashes().forEach(function(row, index){
        promises.push(phonesDisplayed[index].getName().should.eventually.equal(row.phoneName));
      });
      return Q.all(promises);
    }).should.notify(next);
  });


};