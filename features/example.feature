Feature: Example feature

  @complete
  Scenario: Go to an angular site
    Given I go the angular phonecat site
    Then the page title should be 'Google Phone Gallery' (using plain old chai to make the assertion)
    And the page title should be 'Google Phone Gallery' (using chai-as-promised to make the assertion)

  @complete @beforeScenarioTag
  Scenario: I can see a specific phone is present by using protractor explicitly (with the locators .cssContainingText() and .binding())
    Given I am on the angular phonecat site
    Then the phone 'Motorola XOOM™ with Wi-Fi' is displayed with description 'The Next, Next Generation Experience the future with Motorola XOOM with Wi-Fi, the world's first tablet powered by Android 3.0 (Honeycomb).' (explicit)

  @complete
  Scenario: I can see a specific phone is present by encapulating protractor with page/component objects
    Given I am on the angular phonecat site
    Then the phone 'Motorola XOOM™ with Wi-Fi' is displayed with description 'The Next, Next Generation Experience the future with Motorola XOOM with Wi-Fi, the world's first tablet powered by Android 3.0 (Honeycomb).' (component/page object approach)

  @complete
  Scenario: I can see the following phones listed by using protractor explicitly option 1
    Given I am on the angular phonecat site
    Then the following phones are listed in the order stated (explicit: option 1):
      |phoneName                           |
      |Motorola XOOM™ with Wi-Fi           |
      |MOTOROLA XOOM™                      |
      |MOTOROLA ATRIX™ 4G                  |
      |Dell Streak 7                       |
      |Samsung Gem™                        |
      |Dell Venue                          |
      |Nexus S                             |
      |LG Axis                             |
      |Samsung Galaxy Tab™                 |
      |Samsung Showcase™ a Galaxy S™ phone |
      |DROID™ 2 Global by Motorola         |
      |DROID™ Pro by Motorola              |
      |MOTOROLA BRAVO™ with MOTOBLUR™      |
      |Motorola DEFY™ with MOTOBLUR™       |
      |T-Mobile myTouch 4G                 |
      |Samsung Mesmerize™ a Galaxy S™ phone|
      |SANYO ZIO                           |
      |Samsung Transform™                  |
      |T-Mobile G2                         |
      |Motorola CHARM™ with MOTOBLUR™      |

  @complete
  Scenario: I can see the following phones listed by using protractor explicitly option 2
    Given I am on the angular phonecat site
    Then the following phones are listed in the order stated (explicit: option 2):
      |phoneName                           |
      |Motorola XOOM™ with Wi-Fi           |
      |MOTOROLA XOOM™                      |
      |MOTOROLA ATRIX™ 4G                  |
      |Dell Streak 7                       |
      |Samsung Gem™                        |
      |Dell Venue                          |
      |Nexus S                             |
      |LG Axis                             |
      |Samsung Galaxy Tab™                 |
      |Samsung Showcase™ a Galaxy S™ phone |
      |DROID™ 2 Global by Motorola         |
      |DROID™ Pro by Motorola              |
      |MOTOROLA BRAVO™ with MOTOBLUR™      |
      |Motorola DEFY™ with MOTOBLUR™       |
      |T-Mobile myTouch 4G                 |
      |Samsung Mesmerize™ a Galaxy S™ phone|
      |SANYO ZIO                           |
      |Samsung Transform™                  |
      |T-Mobile G2                         |
      |Motorola CHARM™ with MOTOBLUR™      |

  @complete @afterScenarioTag
  Scenario: I can see the following phones listed by encapulating protractor with page/component objects
    Given I am on the angular phonecat site
    Then the following phones are listed in the order stated (component/page object approach):
      |phoneName                           |
      |Motorola XOOM™ with Wi-Fi           |
      |MOTOROLA XOOM™                      |
      |MOTOROLA ATRIX™ 4G                  |
      |Dell Streak 7                       |
      |Samsung Gem™                        |
      |Dell Venue                          |
      |Nexus S                             |
      |LG Axis                             |
      |Samsung Galaxy Tab™                 |
      |Samsung Showcase™ a Galaxy S™ phone |
      |DROID™ 2 Global by Motorola         |
      |DROID™ Pro by Motorola              |
      |MOTOROLA BRAVO™ with MOTOBLUR™      |
      |Motorola DEFY™ with MOTOBLUR™       |
      |T-Mobile myTouch 4G                 |
      |Samsung Mesmerize™ a Galaxy S™ phone|
      |SANYO ZIO                           |
      |Samsung Transform™                  |
      |T-Mobile G2                         |
      |Motorola CHARM™ with MOTOBLUR™      |