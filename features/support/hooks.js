var hooks = function () {

  this.registerHandler('BeforeFeatures', function (event, callback) {
    console.log('Printed/executed before all features.')
    callback();
  });

  this.registerHandler('BeforeFeature', function (event, callback) {
    console.log('Printed/executed before each feature.')
    callback();
  });

  this.Before(function(scenarioName, callback) {
    // Just like inside step definitions, "this" is set to a World instance.
    // It's actually the same instance the current scenario step definitions
    // will receive.

    console.log('Printed/executed before each scenario.');

    // scenarioName is an optional parameter that be passed to any of the before/after scenario methods.
    // For more information see - https://github.com/cucumber/cucumber-js/blob/master/lib/cucumber/ast/scenario.js
    console.log('Scenario name: ' + scenarioName.getName());
    callback();
  });

  this.Before('@beforeScenarioTag', function(callback) {
    console.log('Printed/executed before each scenario tagged with @beforeScenarioTag');
    callback();
  });

  this.After(function(callback) {
    // Just like inside step definitions, "this" is set to a World instance.
    // It's actually the same instance the current scenario step definitions
    // will receive.

    // This hook could be used to clear a database table, etc
    console.log('Printed/executed after each scenario.');
    callback();
  });

  this.After('@afterScenarioTag', function(callback) {
    console.log('Printed/executed after each scenario tagged with @afterScenarioTag');
    callback();
  });

  this.registerHandler('AfterFeature', function (event, callback) {
    console.log('Printed/executed after each feature.')
    callback();
  });

  this.registerHandler('AfterFeatures', function (event, callback) {
    console.log('Printed/executed after all features have been completed.')
    callback();
  });
};

module.exports = hooks;