module.exports = function ListedPhone(phoneName, elem) {

  var phoneElement;

  if (elem){
    phoneElement = elem;
  }else{
    phoneElement = element(by.cssContainingText('.phone-listing', phoneName));
  }


  this.getElement = function () {
    return phoneElement;
  },

  this.isDisplayed = function () {
    return phoneElement.isDisplayed();
  },

  this.getName = function () {
    return phoneElement.element(by.binding('phone.name')).getText();
  };

  this.getDescription = function () {
    return phoneElement.element(by.binding('phone.snippet')).getText();
  };

};