#Protractor Cucumber example
Follow the instructions below to set up a skeleton test automation framework that uses Protractor and Cucumber.

In order to demonstrate how Protractor and Cucumber work example tests have been provided. If you don't wish to see example tests then delete any file beginning with example and the folder angular-phonecat.

##What this is not
This isn't a good example of how to use Given, When, Then.

##Set up Protractor
1. Install node for your platform, either using the prebuilt installer http://nodejs.org/download/ or using your platform specific package installer (https://github.com/joyent/node/wiki/installing-node.js-via-package-manager). For OSX I installed node using Homebrew with the command:
brew install node
2. Install grunt's command line interface:
npm install -g grunt-cli
3. Install all project dependencies:
npm install
4. Download the latest selenium jar and chromedriver:
./node_modules/protractor/bin/webdriver-manager update

##Execute tests
#####If you wish to see the example tests in action then ensure you start up the Angular phonecat project as follows:
1. cd angular-phonecat
2. npm start
3. Verify the app is running at: http://localhost:8000/app/index.html

###Run tests (without grunt)
./node_modules/protractor/bin/protractor conf.js

###Run tests (with grunt)
###### You have 2 options:
1. grunt protractor:default - this runs tests against protractor's default browser chrome.
2. grunt protractor:[browser] where [browser] can be chrome, firefox or safari

##Run tests in parallel
In order to run tests in parallel you will need to update the conf.js file and make use of the `capabilities : {}` object if you want to run tests in parallel against one browser or
the `multiCapabilities : [capabilities: {}, capabilities: {}, and so on]` array if you want to run tests in parallel against more than one browser.

##Intellij/WebStorm users

###Plugins
1. Install cucumber.js plugin. This allows you to use the Intellij command+B (OSX) to quickly jump into a step definition from the feature file.

###To run/debug via the IDE
1. Go to Edit configurations
2. Select + and choose node.js
3. Name= whatever you want to call this configuration, e.g. protractor
4. JavaScript File: node_modules/protractor/lib/cli.js
5. Application parameters: conf.js
4. Click OK
5. Click Run or Debug (adding breakpoints where necessary)