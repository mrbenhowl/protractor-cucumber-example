exports.config = {

    // For more config options see:
    // https://github.com/angular/protractor/blob/master/docs/referenceConf.js


    // The following property starts a standalone Selenium server locally, it is also possible to
    // connect to a Selenium server already running, use saucelabs or connect directly to a browser's driver.
    seleniumServerJar: 'node_modules/protractor/selenium/selenium-server-standalone-2.44.0.jar',


    // Spec/feature patterns are relative to the location of this config.
    specs: [
        'features/*.feature'
    ],


    // Spec/feature patterns to exclude.
    exclude: [],


    // A base URL for your application under test. Calls to protractor.get()
    // with relative paths will be prepended with this.
    baseUrl: 'http://localhost:8000',


    // A callback function called once protractor is ready and available, and
    // before the specs are executed.
    // If multiple capabilities are being run, this will run once per
    // capability.
    // You can specify a file containing code to run by setting onPrepare to
    // the filename string.
    onPrepare: function(){
        //
        // Make chai assertion library available globally
        var chai = require('chai');
        chai.use(require('chai-as-promised'));
        chai.use(require('chai-things'));
        chai.should();

        Object.defineProperty(protractor.promise.Promise.prototype, 'should', {
            get: Object.prototype.__lookupGetter__('should'),
            set: Object.prototype.__lookupSetter__('should')
        });
    },

    // Options to be passed to Cucumber.
    framework: 'cucumber',
    cucumberOpts: {
        tags: '@complete',      // Run only scenarios marked with @complete tag. Also possible to specify a number of tags to include or exclude, e.g. ['@complete', '~@ignore']
        format: 'json'          // other format options are 'pretty' and 'summary'
    }

    // Parallel test execution
    // See the example config file referenced above for setting up tests in parallel
    // Use capabilities : {} if running just one browser
    // Use multiCapabilities: [capabilities : {}, capabilities : {} and so on] for multiple browsers
};